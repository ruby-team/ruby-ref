ruby-ref (2.0.0-2) unstable; urgency=medium

  * Team upload.

  [ Cédric Boutillier ]
  * Remove version in the gem2deb build-dependency
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.

  [ Andreas Tille ]
  * Fix clean target
    Closes: #1048572
  * Standards-Version: 4.7.0 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)
  * Drop deprecated {XS,XB}-Ruby-Versions fields (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * watch file standard 4 (routine-update)
  * Drop ruby-interpreter from Depends

 -- Andreas Tille <tille@debian.org>  Fri, 17 Jan 2025 10:22:41 +0100

ruby-ref (2.0.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
  * Remove un-needed dfsg (jar files are not inclued by upstream anymore).
  * Update patchs and files names according new release.
  * Add simplecov, rspec and coverall to Build-Deps.

 -- Sebastien Badia <seb@sebian.fr>  Wed, 19 Aug 2015 21:10:50 +0200

ruby-ref (1.0.5+dfsg-2~exp1) experimental; urgency=medium

  * Team upload.
  * Target experimental and build with ruby 2.2.
  * Add ruby-test-unit to Build-Depends for ruby2.2.
  * Update Vcs-Browser to cgit URL and HTTPS.
  * Bump Standards-Version to 3.9.6 (no further changes).

 -- Sebastien Badia <seb@sebian.fr>  Mon, 20 Apr 2015 00:11:50 +0200

ruby-ref (1.0.5+dfsg-1) unstable; urgency=low

  * Team upload.

  [ Markus Tornow ]
  * New upstream release.
  * Patching test/weak_reference_test.rb. (Closes: #746146)

  [ Caitlin Matos ]
  * Bump Standards-Version to 3.9.5. No changes were required.
  * Bump Debhelper compat level to 9.
  * Update dates in d/copyright.

 -- Caitlin Matos <caitlin.matos@zoho.com>  Sat, 26 Jul 2014 03:16:53 -0400

ruby-ref (1.0.2+dfsg-1) unstable; urgency=low

  [ Markus Tornow ]
  * Initial release (Closes: #698448)

  [ Cédric Boutillier ]
  * Repack upstream tarball to remove .jar file
  * Override lintian message about missing upstream changelog

 -- Markus Tornow <tornow@riseup.net>  Sun, 16 Dec 2012 05:44:08 +0100
